#include <stdio.h>


int main ( void )
{
    int a, b, c, d;
    printf("Zadej dva casy oddelene mezerou: ");
    scanf("%d:%d %d:%d", &a, &b, &c, &d);
    int mcas1 = (a*60) + b;
    int mcas2 = (c*60) + d;
    int diffminutes;
    if (mcas2 > mcas1)
        diffminutes = mcas2 - mcas1;
    else 
        diffminutes = (60*24 - mcas1) + mcas2;
    printf("%d:%02d", diffminutes / 60, diffminutes % 60);
    return 0;
}