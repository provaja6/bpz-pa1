#include <stdio.h>
#include <time.h>
#include <stdlib.h>

// Napište funkci, která seřadí pole pomocí Bubble sort

#define SIZE 10

void bubbleSort(int *arr, int n) {
    int i, j;
    for (i = 0; i < n - 1; i++) {
        for (j = 0; j < n - i - 1; j++) {
            if (arr[j] > arr[j + 1]) {
                //swap
                int tmp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = tmp;
            }
        }
    }
}

void printArray(int *arr, int n) {
    for (int i = 0; i < n; ++i) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main(void) {
    srand(time(NULL));
    int array[SIZE];
    for (int i = 0; i < SIZE; ++i) {
        array[i] = rand() % 100;
    }

    printf("Original array:\n");
    printArray(array, SIZE);
    bubbleSort(array, SIZE);
    printf("Sorted array:\n");
    printArray(array, SIZE);

    return 0;
}