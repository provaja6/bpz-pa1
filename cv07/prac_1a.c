#include <stdio.h>
#include <time.h>
#include <stdlib.h>

// Napište funkci, která zamíchá hodnoty v poli náhodně

#define SIZE 10

void shuffleArray(int *arr, int n) {
    for (int i = 0; i < n; ++i) {
        int x = rand() % 10;
        int y = rand() % 10;
        //swap
        int c = arr[x];
        arr[x] = arr[y];
        arr[y] = c;
    }
}

void printArray(int *arr, int n) {
    for (int i = 0; i < n; ++i) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main(void) {
    srand(time(NULL));
    int array[SIZE];
    for (int i = 0; i < SIZE; ++i) {
        array[i] = i + 1;
    }

    printf("Original array:\n");
    printArray(array, SIZE);
    shuffleArray(array, SIZE);
    printf("Shuffled array:\n");
    printArray(array, SIZE);

    return 0;
}