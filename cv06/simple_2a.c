#include <stdio.h>

// Napište funkci (proceduru), která vypíše na obrazovku n hvězdiček. Využijte ji ve funkci pro vykreslení
// obdélníku (zadaného 2 parametry)


void printRow(int n) {
    for (int i = 0; i < n; ++i) {
        printf("*");
    }
    printf("\n");
}


void printColumn(int a) {
    printf("*");
    for (int i = 0; i < a - 2; ++i) {
        printf(" ");
    }
    printf("*\n");
}

void printRectangle(int a, int b) {
    printRow(a);
    for (int i = 0; i < b - 2; ++i) {
        printColumn(a);
    }
    printRow(a);
}


int main(void) {
    int a, b;
    if (scanf("%d %d", &a, &b) != 2 || a < 2 || b < 2) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    printRectangle(a, b);
    return 1;
}
