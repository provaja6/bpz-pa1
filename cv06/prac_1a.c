#include <stdio.h>

// Napište funkci, která vypočte x^n, kde x i n jsou celá čísla - triviálně

int input(int *x, int *n) {
    if (scanf("%d %d", x, n) != 2 || n < 0) {
        return 0;
    }
    return 1;
}

int power(int x, int n) {
    int p = 1;

    for (int i = 0; i < n; ++i) {
        p *= x;
    }
    return p;
}

int main(void) {
    int x, n, p;
    if (!input(&x, &n)) {
        printf("Nespravny vstup.\n");
        return 1;
    }
    p = power(x, n);

    printf("%d^%d = %d\n", x, n, p);
    return 0;
}