#include <stdio.h>

// Napište funkci pro iterativní výpočet faktoriálu.

int input(int *n) {
    if (scanf("%d", n) != 1 || n < 0) {
        return 0;
    }
    return 1;
}

int factorial(int n) {
    int fact = 1;

    for (int i = 1; i <= n; ++i) {
        fact = fact * i;
    }
    return fact;
}

int main(void) {
    int n, res;
    if (!input(&n)) {
        printf("Nespravny vstup.\n");
        return 1;
    }

    res = factorial(n);

    printf("%d! = %d\n", n, res);
    return 0;
}