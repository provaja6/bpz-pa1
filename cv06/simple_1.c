#include <stdio.h>

//Napište funkci (proceduru), která vypíše na obrazovku 10 hvězdiček.

#define STARS 10

void printStars() {
    for (int i = 0; i < STARS; ++i) {
        printf("*");
    }
    printf("\n");
}

int main(void) {
    printStars();
    return 0;
}