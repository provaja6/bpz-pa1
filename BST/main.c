#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>

#include "tree.h"

/*
 * Work with binary search tree(BST)
 */

// Returning values
#define EMPTY 2
#define TRUE 1
#define FALSE 0

// Constants
#define SECONDS 1
#define READ_OPERATIONS 3
#define WRITE_OPERATIONS 3

/*
 * Global constants of character symbols an operation on the tree
 */
const char readTreeOperatins[READ_OPERATIONS] = { '^', '<', '>' };
const char writeTreeOperatins[WRITE_OPERATIONS] = { '+', '-', '?' };

/**
 * Read an integer until get valid value or EOF
 * 
 * @param[in,out] value A pointer to integer where value will be stored
 * @return 1(TRUE) if the value was succesfull read or -1(EOF)
 */
int readValue(int *value) {
    int scannedValue;

    while( TRUE ) {
        scannedValue = scanf("%d", value);

        if( scannedValue == 1 && 
            *value > INT_MIN && 
            *value < INT_MAX ) {
            return TRUE;
        } 

        if( scannedValue == EOF ) {
            return EOF;
        } 

        if( scannedValue == FALSE ) {
            printf("Wrong value.\n"
                   "The value should in range between %d and %d.\n"
                   "Try again.\n", INT_MIN, INT_MAX);
        } 
    }
}

/**
 * Check if a string(array of chars) contains a character 
 * 
 * @param[in] operations A string
 * @param[in] sizeOfArray A size of the string
 * @param[in, out] character The searching character 
 * @return 1(TRUE) when the string contains the character otherwise 0(FALSE)
 */
int arrayContains(const char operations[], int sizeOfArray, char *character) {
    int contains = FALSE;

    for( int i = 0; i < sizeOfArray; ++i ) {
        if( operations[i] == *character ) {
            return TRUE;
        }
    }

    return contains;
}

/**
 * Read an character of a operation on the tree
 * 
 * @param[in,out] operation A pointer to char where the character 
 *                          of the operation will be stored
 * @return 2(TRUE) when was read a write operation
 *         1(TRUE) when was read a read operation 
 *         0(FALSE) when the char is not known operation or 
 *         -1(EOF)
 */
int readOperation(char *operation) {
    int scannedValue = scanf(" %c", operation);

    if( scannedValue == EOF ) {
        return EOF;
    } else if( scannedValue == 1 && 
               arrayContains(readTreeOperatins, READ_OPERATIONS, operation) ) {
        return TRUE;
    } else if( scannedValue == 1 && 
               arrayContains(writeTreeOperatins, WRITE_OPERATIONS, operation) ) {
        return TRUE + 1;
    } else {
        return FALSE;
    }
}

/**
 * Print help about the tree operations
 */
void printHelp() {
    printf("\n");
    printf("+ <number>\tInsert a node into tree with the key\n");
    printf("- <number>\tRemove a node with the key\n");
    printf("? <number>\tAsk if the tree contains a node with key\n");
    printf("^         \tPrint the tree\n");
    printf("<         \tPrint the minimum of the tree\n");
    printf(">         \tPrint the maximum of the tree\n");
    printf("\n");
}

/**
 * Process operations on tree which does not change the tree
 * Read-only operation
 * 
 * @param[in,out] root A pointer of pointer to the root of the tree
 * @param[in] operation A pointer to char of operation
 * @return 2(EMPTY) when the root pointer is NULL
 *         1(TRUE) when the process succesfull end
 *         0(FALSE) when the char is not known operation or 
 */
int processTreeReadOperation(Node **root, char *operation) {
    if( root == NULL ) {
        return EMPTY;
    }

    switch( *operation ) {
        case '^':
            printTreeIn2D(*root);
            printf("\n");
            return TRUE;

        /*
         * These two case for '<' and '>' are separate in blocks
         * because there is an initialization of properties
         * which should belongs to one specific case
         */
        case '<': {
            Node* min = getMinimumKeyOfTree(*root);

            if( min != NULL ) {
                printf("The minum of the tree is %d\n\n", min->key);
            } else {
                printf("The tree has NO child\n\n");
            }

            return TRUE;
        }

        case '>': {
            Node* max = getMaxKeyOfTree(*root);

            if( max != NULL ) {
                printf("The maximum of the tree is %d\n\n", max->key);
            } else {
                printf("The tree has NO child\n\n");
            }

            return TRUE;
        }
    
        default:
            return FALSE;
    }
} 

/**
 * Process operations on tree which change the tree
 * Write operation
 * 
 * @param[in,out] root A pointer of pointer to the root of the tree
 * @param[in] operation A pointer to char of operation
 * @param[in] value A pointer to integer which is used to operate on tree
 * @return 1(TRUE) when the process succesfull end, 
 *         0(FALSE) when the char is not known operation or 
 *         -2 when the root pointer is NULL
 */
int processTreeWriteOperation(Node **root, char *operation, int *value) {
    if( root == NULL ) {
        return -2;
    }

    switch( *operation ) {
        case '?':
            if( containsTree(*root, *value) ) {
                printf("The tree containts the key %d\n\n", *value);
            } else {
                printf("The tree DOESN'T containt the key %d\n\n", *value);
            }
            return TRUE;

        case '+':   
            return insertIntoTree((*root), *value);

        case '-':
            *root = removeNodeInTree(*root, *value);
            return TRUE;

        default:
            return FALSE;
    }
}

/**
 * Read value and set it as root of the tree
 * 
 * @param[in,out] root A pointer of pointer to the root of the tree
 * @return 1(TRUE) then the initialization successful ended or 
 *         -1(EOF)
 */
int initTree(Node **root) {
    int value;
        
    if( readValue(&value) == EOF ) {
        return EOF;
    }

    printf("\n");
    *root = newTreeNode(value);

    return TRUE;
}

/**
 * Work with tree
 * 
 * @param[in,out] root A pointer to the root of the tree
 */
void workWithTree(Node *root) {
    char operation;
    int value;

    printf("At first need to initialize a root of the root.\n"
           "Please write a key for it.\n");
        
    if( initTree(&root) == EOF ) {
        printf("End of inputs.\n");
        return;
    }

    while( TRUE ) { 
        // Read an operation
        int operationReturned = readOperation(&operation);
        if( operationReturned == EOF ) {
            printf("End of inputs.\n");
            break;
        }

        if( operationReturned == 0 ) {
            printf("Unknown operation.\n"
                   "Try again\n");
            sleep(SECONDS);
            printHelp();
            continue;
        }

        // Process a read operation on the tree
        if( operationReturned == 1 ) {
            int ret = processTreeReadOperation(&root, &operation);
            if( ret == 0 ) {
                printf("proccessTreeReadOperation: Opps... something went wrong\n");
                continue;
            }

            if( ret == -2 ) {
                printf("The tree is empty\n");
                continue;
            }

        } else {
            // Process a write operation on the tree
            int valueReturned = readValue(&value);
            if( valueReturned == EOF ) {
                printf("End of inputs.\n");
                break;
            }

            int proccessReturned = processTreeWriteOperation(&root, &operation, &value);
            if( proccessReturned == 0 ) {
                printf("proccessTreeWriteOperation: Opps... something went wrong\n");
                continue;
            }

            if( proccessReturned == -2 ) {
                printf("The tree is empty\n");
                continue;
            }

            if( root == NULL ) {
                printf("The tree is empty.\n"
                        "Before anything else we should initialize it.\n"
                        "Write a value.\n");

                if( initTree(&root) == EOF ) {
                    printf("End of inputs.\n");
                    return;
                }
            }
        }
    } 

    freeTree(root);
}

int main() {
    Node *tree = NULL;

    printf("Let's work with binary search tree aka BST\n");

    // Wait a few seconds before anything
    sleep(SECONDS);
    printHelp();

    workWithTree(tree);

    return 0;
}
