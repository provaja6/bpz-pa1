#!/bin/sh

g++ -Wall -pedantic -g main.c -o main.o -c
g++ -Wall -pedantic -g tree.c -o tree.o -c
g++ -g -o bst main.o tree.o
