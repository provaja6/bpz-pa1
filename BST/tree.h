/*
 * Declarations of operations of basic binary search tree(BST)
 * https://en.wikipedia.org/wiki/Binary_search_tree
 */


// Struct represents a node of a binary search tree(BTS)
typedef struct Node {
    // A value as key of the node
    int key;

    // A pointer to the left child node
    struct Node *left;
    // A pointer to the right child node
    struct Node *right;
} Node;

/**
 * Check if the tree contains a node with the key
 * 
 * @param[out] root A pointer to the root of the tree which isn't changeable
 * @param[out] key A key of the searching node
 * @return 1(TRUE) if the tree contains a node with the key otherwise 0(FALSE)
 */
int containsTree(const Node *root, int key);

/**
 * Free(dealloc) memory of the tree  
 * 
 * @param[in,out] root A pointer to the root of the tree
 */
void freeTree(Node *root);

/**
 * Insert a key in the tree
 * 
 * @param[in,out] root A pointer to the root of the tree
 * @param[out] key A key of a new node
 * @return 1(TRUE) when a node with the key was succesfull created,
 *         0(FALSE) when the tree already contains a node with the key or
 *         -2 when the tree is empty
 */
int insertIntoTree(Node *root, int key);

/**
 * Find a node in the tree with the smallest key
 * 
 * @param[out] root A pointer to the root of the tree
 * @return A pointer of the node with the smallest key or NULL if tree has no node
 */
Node *getMinimumKeyOfTree(Node *root);

/**
 * Find a node in the tree with the greatest key
 * 
 * @param[out] root A pointer to the root of the tree
 * @return A pointer of the node with the greatest key or NULL if tree has no node
 */
Node *getMaxKeyOfTree(Node *root);

/**
 * Find a node in the tree with the key
 * 
 * @param[out] root A pointer to the root of the tree
 * @param[out] key A key of the searching node
 * @return A pointer of the node with key or NULL if the key wasn't found
 */
Node *getNodeWithKeyInTree(Node *root, int key);

/**
 * Create a new node of a tree
 * 
 * @param[out] key A key of the new node
 * @return A pointer to the node
 */
Node *newTreeNode(int key);

/**
 * Print values of the tree
 * 
 * @param[out] root A pointer to the root of the tree
 * @param[out] filter A flag of a filter if:
 *               1 print values in ascending order
 *               2 print values in descending order
 */
void printTree(Node *root, int filter);

/**
 * Wrapper over print2DUtil()
 */
void printTreeIn2D(Node *root);

/**
 * Print values of the tree in 2D
 * 
 * @param[out] root A pointer to the root of the tree
 * @param[out] space Count of a spaces between each elements
 */
void printTreeIn2DWithSpaces(Node *root, int space);

/**
 * Remove a node with the specific key
 * note: The function was taken from this webiset
 * https://www.log2base2.com/data-structures/tree/remove-an-element-from-binary-search-tree.html
 * 
 * @param[out] root A pointer to the root of the tree
 * @param[out] key A key of the removing node
 * @return The actual root's address or NULL: 
 *         1. If we send the empty tree
 *         2. If the given node is not present in the tree
 */
Node *removeNodeInTree(Node *root, int key);
