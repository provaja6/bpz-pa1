# Binary search tree

An implementation of trivial Binary Search Tree. This task is for a inspiration how to write reusable and readable code.


## How to use it:

### Header file
For implementation of the BST. There is a header file **tree.h** where is a declaration of one node of the tree and functions for work with this tree.

### C file
Then there a c file **tree.c** where is the specific implementation of functions from header file.

### Main file
Finally there is a main file *main.c* where you find some interactive flow how to try the BST.

This a great example how implement something for reusing because you can easily take tree files and use in another program.

### Run:
For compiling:
```shell
./run.sh
```

For a executing:
```shell
./bst
```

For running tests:
```shell
./test.sh
```


## TODO:
- [ ] implement a function for a merge two trees
- [ ] implement a function for a intersect of two trees
- [ ] implement a function for balancing tree([AVL](https://en.wikipedia.org/wiki/AVL_tree)) 
- [ ] implement a red black tree([RBT](https://en.wikipedia.org/wiki/Red–black_tree)), which is more effitient then AVL
- [ ] maybe add reading hints from file
- [ ] refractor "workWithTree" function
...


> **_NOTE:_** If you have any idea how to improve it, feel free to create a new branch, implemented it and create merge request or directly contact us.
The same like if you find some bugs, mistakes or some unclear parts.
