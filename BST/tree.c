#include <stdio.h>
#include <stdlib.h>

#include "tree.h"

/*
 * Implentation of operations of basic binary search tree(BST)
 * https://en.wikipedia.org/wiki/Binary_search_tree
 */

// Returning values
#define EMPTY 2
#define TRUE 1
#define FALSE 0

// Constants
#define COUNT 3


/**
 * Check if the tree contains a node with the key
 * 
 * @param[out] root A pointer to the root of the tree which not changeable
 * @param[out] key A key of the searching node
 * @return 1(TRUE) if the tree contains a node with the key otherwise 
 *         0(FALSE)
 */
int containsTree(const Node *root, int key) {
    if( root == NULL ) {
        return FALSE;
    }

    if( key == root->key ) {
        return TRUE;
    } else if( (root->left != NULL) && (key < root->key) ) {
        return containsTree(root->left, key);
    } else if( (root->right != NULL) && (key > root->key) ) {
        return containsTree(root->right, key);
    } else {
        return FALSE;
    }
}

/**
 * Free(dealloc) memory of the tree  
 * 
 * @param[in,out] root A pointer to the root of the tree
 */
void freeTree(Node *root) {
    if( root == NULL ) {
        return;
    }

    // Go recursively through all nodes
    freeTree(root->left);
    freeTree(root->right);

    // When going back free(dealloc) memory
    free(root);
}

/**
 * Insert a key in the tree
 * 
 * @param[in,out] root A pointer to the root of the tree
 * @param[out] key A key of a new node
 * @return 1(TRUE) when a node with the key was succesfull created,
 *         0(FALSE) when the tree already contains a node with the key or
 *         2(EMPTY) when the tree is empty
 */
int insertIntoTree(Node *root, int key) {
    if( root == NULL ) {
        return EMPTY;
    }

    if( key == root->key) {
        return FALSE;
    } else if( key <= root->key ) {
        if( root->left == NULL ) {
            root->left = newTreeNode(key);
        } else {
            return insertIntoTree(root->left, key);
        }
    } else {
        if( root->right == NULL ) {
            root->right = newTreeNode(key);
        } else {
            return insertIntoTree(root->right, key);
        }
    }

    return TRUE;
}

/**
 * Find a node in the tree with the smallest key
 * 
 * @param[out] root A pointer to the root of the tree
 * @return A pointer of the node with the smallest key or NULL if tree has no node
 */
Node *getMinimumKeyOfTree(Node *root) {
    // min value should be present in the leftmost node
    while( root->left != NULL ) { 
        root = root->left;
    }

    return root;
}

/**
 * Find a node in the tree with the greatest key
 * 
 * @param[out] root A pointer to the root of the tree
 * @return A pointer of the node with the greatest key or NULL if tree has no node
 */
Node *getMaxKeyOfTree(Node *root)  {
    // max value should be present in the rightmost node
    while( root->right != NULL ) {
        root = root->right;
    }

    return root;
}

/**
 * Find a node in the tree with the key
 * 
 * @param[out] root A pointer to the root of the tree
 * @param[out] key A key of the searching node
 * @return A pointer of the node with key or NULL if the key wasn't found
 */
Node *getNodeWithKeyInTree(Node *root, int key) {
    if( root == NULL ) {
        return NULL;
    }

    if( key == root->key ) {
        return root;
    } else if( (root->left != NULL) && (key < root->key) ) {
        return getNodeWithKeyInTree(root->left, key);
    } else if( (root->right != NULL) && (key > root->key) ) {
        return getNodeWithKeyInTree(root->right, key);
    } else {
        return NULL;
    }
}

/**
 * Create a new node of a tree
 * 
 * @param[out] key A key of the new node
 * @return A pointer to the node
 */
Node *newTreeNode(int key) {
    Node *node = (Node*) malloc (sizeof(Node));

    node->key = key;
    node->left = NULL;
    node->right = NULL;

    return node;
}

/**
 * Print values of the tree
 * 
 * @param[out] root A pointer to the root of the tree
 * @param[out] filter A flag of a filter if:
 *               1 print values in ascending order
 *               2 print values in descending order
 */
void printTree(Node *root, int filter) {
    if( root == NULL ) {
        printf("\nThe tree is empty.\n");
        return;
    } 

    if( filter == 1 ) {
        /*
         * At first find the smallest key 
         * which is the leftmost node
         * All value is stored in heap
         */
        printTree(root->left, filter);
    } else {
        /*
         * At first find the greatest key 
         * which is the rightmost node
         * All value is stored in heap,
         */
        printTree(root->right, filter);
    }

    /*
     * Print a node based on filter
     * Then go to the node on the other subtree 
     * and print it as well
     */
    printf("%d\n", root->key);
    
    if( filter == 1 ) {
        printTree(root->right, filter);
    } else {
        printTree(root->left, filter);
    }
}

/**
 * Wrapper over print2DUtil()
 */
void printTreeIn2D(Node *root) {
    if( root == NULL ) {
        printf("\nThe tree is empty.\n");
        return;
    } 
 
    // Pass initial space count as 0
    printTreeIn2DWithSpaces(root, 0);
}

/**
 * Print values of the tree in 2D
 * 
 * @param[out] root A pointer to the root of the tree
 * @param[out] space Count of a spaces between each elements
 */
void printTreeIn2DWithSpaces(Node *root, int space) {
    if( root == NULL ) {
        return;
    }

    // Increase distance between levels
    space += COUNT;
 
    // Process right child
    printTreeIn2DWithSpaces(root->right, space);
 
    // Print tree node after space
    printf("\n");
    for( int i = COUNT; i < space; ++i ) {
        printf(" ");
    }
    printf("%d\n", root->key);
 
    // Process left child
    printTreeIn2DWithSpaces(root->left, space);
}

/**
 * Remove a node with the specific key
 * note: The function was taken from this webiset
 * https://www.log2base2.com/data-structures/tree/remove-an-element-from-binary-search-tree.html
 * 
 * @param[out] root A pointer to the root of the tree
 * @param[out] key A key of the removing node
 * @return The actual root's address or NULL: 
 *         1. If we send the empty tree
 *         2. If the given node is not present in the tree
 */
Node *removeNodeInTree(Node *root, int key) {
    if( root == NULL )
        return NULL;

    if( key > root->key ) {
        root->right = removeNodeInTree(root->right, key);

    } else if( key < root->key ) {
        root->left = removeNodeInTree(root->left, key);
    } else {
        if( (root->left == NULL) && (root->right == NULL) ) {        
            /*
            * Case 1: Leaf node. Both left and right reference is NULL
            * replace the node with NULL by returning NULL to the calling pointer
            * and free the node
            */
            free(root);
            return NULL;
        } else if( root->left == NULL ) {
            /*
             * Case 2: Node has right child.
             * replace the root node with root->right and free the right node
             * and free the node
             */
            Node *temp = root->right;
            free(root);
            return temp;

        } else if( root->right == NULL ) {
            /*
             * Case 3: Node has left child.
             * replace the node with root->left and free the left node
             * and free the node
             */
            Node *temp = root->left;
            free(root);
            return temp;
            
        } else {
            /*
             * Case 4: Node has both left and right children
             * Find the min value in the right subtree
             * replace node value with min
             * And again call the remove function to delete the node which has the min value
             * Since we find the min value from the right subtree call the remove function with root->right
             */
            int rightMin = getMinimumKeyOfTree(root->right)->key;
            root->key = rightMin;
            root->right = removeNodeInTree(root->right, rightMin);
        }
    }

    // return the actual root's address
    return root;
}
