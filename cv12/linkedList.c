#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int m_data;
    struct node* m_next;

} Node;

Node* listCreate() {
    return NULL;
}

int listEmpty(Node* head) {
    if (head == NULL) {
        return true;
    } else {
        return false;
    }
}

void listPushFront(Node** headRef, int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->m_data = data;
    newNode->m_next = *headRef;

    *headRef = newNode;
}

void listPushBack(Node** headRef, int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->m_data = data;
    newNode->m_next = NULL;

    if (listEmpty(*headRef)) {
        *headRef = newNode;
        return;
    }

    Node* current = *headRef;
    while (current->m_next != NULL) {
        current = current->m_next;
    }

    current->m_next = newNode;
    return;
}

void listPrint(Node* head) {
    while (head != NULL) {
        printf("%d ", head->m_data);
        head = head->m_next;
    }
    printf("\n");
}

int listCount(Node* head) {
    int count = 0;
    while (head != NULL) {
        count++;
        head = head->m_next;
    }
    return count;
}

void listPopFront(Node** headRef) {
    if (*headRef == NULL) {
        return;
    }

    Node* tmpHead = *headRef;
    *headRef = (*headRef)->m_next;
    free(tmpHead);
}

void listPopBack(Node** headRef) {
    if (*headRef == NULL) {
        return;
    }

    if ((*headRef)->m_next == NULL) {
        free(*headRef);
        *headRef = NULL;
        return;
    }

    Node* secondLast = *headRef;
    while (secondLast->m_next->m_next != NULL) {
        secondLast = secondLast->m_next;
    }

    free(secondLast->m_next);
    secondLast->m_next = NULL;
}

void listRemove(Node** headRef, int key) {
    Node* tmp = *headRef;
    Node* prev;

    if (*headRef != NULL && (*headRef)->m_data == key) {
        *headRef = (*headRef)->m_next;
        free(tmp);
        return;
    }

    while (tmp != NULL && tmp->m_data != key) {
        prev = tmp;
        tmp = tmp->m_next;
    }

    if (tmp == NULL) {
        return;
    }

    prev->m_next = tmp->m_next;
    free(tmp);
}

void listDelete(Node** headRef) {
    Node* current = *headRef;
    Node* next;
    while (current != NULL) {
        next = current->m_next;
        free(current);
        current = next;
    }
    *headRef = NULL;
}

int main() {
    Node* list = listCreate();
    listPushBack(&list, 2);
    listPushBack(&list, 3);
    listPushBack(&list, 4);
    listPushFront(&list, 1);
    listPushBack(&list, 5);
    listPrint(list);
    printf("count = %d\n", listCount(list));
    listPopFront(&list);
    listPrint(list);
    listPopBack(&list);
    listPrint(list);
    listRemove(&list, 4);
    listPrint(list);
    listRemove(&list, 2);
    listPrint(list);
    listRemove(&list, 99);
    listPrint(list);
    listDelete(&list);

    return 0;
}
