#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "persons.h"

// Expected average count of persons
#define DEFAULT_PERSONS_COUNT 50
// Default of parsed strings
#define DEFAULT_PARSE_STRINGS_COUNT 6
// By google the average lenthg of surname is 46
#define DEFAULT_LENGTH_OF_STRINGS 50


/**
 * Function to alloc an array of pointers to strings(2D chars array).
 * 
 * @param[in] countOfStrings a count of strings to need alloc.
 * @param[in] lengthOfString a length of the one string.
 * @return alloc array of pointers to strings.
 */
char **allocStrings(int countOfStrings, int lengthOfString) {
    char **strings = (char**) malloc (countOfStrings * sizeof(char*));

    for(int i = 0; i < countOfStrings; ++i) {
        strings[i] = (char*) malloc (lengthOfString * sizeof(char));
    }

    return strings;
}

/**
 * Function to free an array of pointers to strings(2D chars array).
 * 
 * @param[in] strings an array of pointers to strings.
 * @param[in] sizeOfStrings a size of alloced strings.
 */
void freeStrings(char **strings, int sizeOfStrings) {
        for(int i = 0; i < sizeOfStrings; ++i) {
        free(strings[i]);
    }

    free(strings);
}

/**
 * Parse a string by delimiter.
 * 
 * @param[in]      line a line to parse.
 * @param[in]      delimiter a delimiter by parse it.
 * @param[in, out] sizeOfStrings a size of alloced strings.
 *                  When the function is call the value is used as default value to alloc an array of pointers to strings.
 *                  When the function finished then in this property is stored the current size of alloced strings.
 * @param[in, out] countOfStrings a count of strings. 
 *                  When the function finished then in this property is stored the current count of strings.
 * @return an array of pointers to parsed strings.
 */
char **parseString(char *line, const char *delimiter, int *sizeOfStrings, int *countOfStrings ) {
    *sizeOfStrings = DEFAULT_PARSE_STRINGS_COUNT;
    int lengthOfStrings = DEFAULT_LENGTH_OF_STRINGS;
    char **strings = allocStrings(*sizeOfStrings, lengthOfStrings);

    *countOfStrings = 0;

    char *token = strtok(line, delimiter);
    while (token) {
        strncpy(strings[*countOfStrings], token, (strlen(token)+1));
        token = strtok(NULL, delimiter);
        ++(*countOfStrings);
    }

    return strings;
}

/**
 * Read input from the standard input stream.
 * 
 * @param[in, out] persons a pointer to an array of persons.
 * @param[in]      sizeOfPersons a size of persons array.
 * @param[in, out] countOfPersons a actual count of persons.
 */
void readInput(Person **persons, int sizeOfPersons, int *countOfPersons) {
    char *line = NULL;
    size_t bufferSize;
    int countOfStrings;

    while( getline(&line, &bufferSize, stdin) != EOF ) {
        int sizeOfStrings;
        char **strings = parseString(line, " ,.", &sizeOfStrings, &countOfStrings);

        if( *countOfPersons >= sizeOfPersons ) {
            break;
        }

        if( !retrievePerson(&(*persons)[*countOfPersons], strings, countOfStrings) ) {
            printf("Nespravny vstup.");
            break;
        }

        ++(*countOfPersons);
        freeStrings(strings, sizeOfStrings);
    }

    printf("Succesfully read %d persons.\n\n", *countOfPersons);
    free(line);
}

// Global property for quick sort parameters comparator.
// To set sort priority and order.
SortFilter *filters = NULL;
// Global property for count of global properties.
int countOfFilters = 0;

void parseArguments(int argc, char *argv[]) {
    // Count of filters is count of arguments minus 1 which is the commmand for executing program e.g. a.out.
    countOfFilters = argc - 1;
    filters = (SortFilter*) malloc (countOfFilters * sizeof(SortFilter));

    for(int i = 0, j = 1; i < countOfFilters; ++i, ++j) {
        int countOfStrings = 0;
        int sizeOfStrings = 0;
        char **strings = parseString(argv[j], " ,", &sizeOfStrings, &countOfStrings);

        if( countOfStrings < 2 ) 
            continue;

        SortFilter *filter = &filters[i];
        if( !mapSortPriority(&filter->priority, strings[0]) || 
            !mapSortOrder(&filter->order, strings[1])
        ) {
            countOfFilters -= 1;
            freeStrings(strings, sizeOfStrings);
            return;
        }

        freeStrings(strings, sizeOfStrings);
    }
}

/* 
 * For filter for sorting expect format:
 * <property_name>, <sort_order> e.g. "surname, ascending"
 */
int main(int argc, char *argv[]) {  
    parseArguments(argc, argv);

    int sizeOfPersons = DEFAULT_PERSONS_COUNT, countOfPersons = 0;
    Person *persons = allocPersons(sizeOfPersons);
    readInput(&persons, sizeOfPersons, &countOfPersons);

    printPersons(persons, countOfPersons);

    sortPersons(&persons, countOfPersons, filters, countOfFilters);

    printf("\nSorted:\n");
    printPersons(persons, countOfPersons);

    freePersons(&persons, sizeOfPersons);
    free(filters);

    return 0;
}
