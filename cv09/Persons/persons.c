#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "persons.h"

// Expected average count of persons
#define DEFAULT_SIZE_OF_STRINGS 50
// By google the average lenthg of surname is 46
#define DEFAULT_LENGTH_OF_STRINGS 50


/**
 * Convert a string to a enum of gender.
 * 
 * @param[in, out] gender a pointer to a gender where values will be stored.
 * @param[in]      strings an array of pointers to strings where values is looked for.
 * @return true when could get the gender otherwise false.
 */
bool convertStringToGender(Gender *gender, const char *string) {
    if( gender == NULL || 
        string == NULL ) {
        return false;
    }

    switch(*string) {
    case 'F': 
        *gender = female;
        break;

    case 'M':
        *gender = male;
        break;

    default: 
        *gender = unknown;
        break;
    }

    return true;
}

/**
 * Convert a gender to a string.
 * 
 * @param[in, out] gender a unchangeable pointer to a gender.
 * @return a pointer to the string of the gender.
 */
char convertGenderToString(const Gender *gender) {
    if( gender == NULL ) {
        return 'X';
    }

    switch(*gender) {
    case female: return 'F';
    case male: return 'M';
    default: return '-';
    }
}

/**
 * Get a datetime from a strings
 * 
 * @param[in, out] date a pointer to a date where values will be stored.
 * @param[in]      strings an array of pointers to strings where values is looked for.
 * @return true when could get the datetime otherwise false.
 */
bool retrieveDateTime(DateTime *date, char **strings) {
    if( strings[2] == NULL || 
        strings[3] == NULL ||
        strings[4] == NULL ) {
        return false;
    }

    date->tm_mday = atoi( strings[2] );
    date->tm_mon = atoi( strings[3] );
    date->tm_year = atoi( strings[4] );

    return true;
}

/**
 * Get a person from a strings
 * 
 * @param[in, out] person a pointer to a person where values will be stored.
 * @param[in]      strings an constant array of pointers to strings where values is looked for.
 * @param[in]      countOfStrings a count of strings. 
 * @return true when could get the person properties otherwise false.
 */
bool retrievePerson(Person *person, char **strings, int countOfStrings) {
    if( countOfStrings >= DEFAULT_SIZE_OF_STRINGS ||
        person == NULL ) {
        return false;
    }

    for(int i = 0; i < countOfStrings; ++i) {
        if( strings[i] == NULL ) {
            return false;
        }
    }

    strncpy( person->name, strings[0], (strlen(strings[0]) + 1) );
    strncpy( person->surname, strings[1], (strlen(strings[1]) + 1) );

    if( !retrieveDateTime(person->birthDate, strings) ) {
        return false;
    }

    if( !convertStringToGender(&(person->gender), strings[5]) ) {
        return false;
    }

    return true;
}

/**
 * Alloc persons.
 * 
 * @param[in] numberOfPersons a number of persons.
 * @return an array of persons.
 */
Person *allocPersons(int numberOfPersons) {
    Person *persons = (Person*) malloc (numberOfPersons * sizeof(Person));

    for(int i = 0; i < numberOfPersons; ++i) {
        persons[i].name = (char*) malloc (DEFAULT_LENGTH_OF_STRINGS * sizeof(char));
        persons[i].surname = (char*) malloc (DEFAULT_LENGTH_OF_STRINGS * sizeof(char));
        persons[i].birthDate = (DateTime*) calloc (1, sizeof(DateTime));
        persons[i].gender = unknown;
    }

    return persons;
}

/**
 * Free persons.
 * 
 * @param[in, out] persons a pointer to an array of persons.
 * @param[in]      numberOfPersons a number of persons.
 */
void freePersons(Person **persons, int numberOfPersons) {
    for(int i = 0; i < numberOfPersons; ++i) {
        free( (*persons)[i].name ); 
        free( (*persons)[i].surname ); 
        free( (*persons)[i].birthDate ); 
    }

    free(*persons);
}

/**
 * Print formatted person's properties.
 * 
 * @param[in, out] person a constant pointer to a person.
 */
void printPerson(const Person *person) {
    if( person == NULL ) {
        return;
    }

    printf("%s %s, ", person->name, person->surname);
    printf("%d.%d.%d, ", person->birthDate->tm_mday, person->birthDate->tm_mon, person->birthDate->tm_year);
    printf("%c\n", convertGenderToString(&person->gender));
}

/**
 * Print formatted persons.
 * 
 * @param[in, out] persons a constant pointer to an array of persons.
 * @param[in]      numberOfPersons a number of persons.
 */
void printPersons(const Person *persons, int numberOfPersons) {
    for(int i = 0; i < numberOfPersons; ++i) {
        printPerson(&persons[i]);
    }
}

/**
 * Convert a string to priority of sort.
 * 
 * @param[in, out] priority a pointer to a priority of sort.
 * @param[in]      string a constat string.
 * @return true when could find a priority of sort to string otherwise false;
 */
bool mapSortPriority(SortPriority *priority, const char *string) {
    if( string == NULL ) {
        return false;
    }

    if( strcmp(string, personPropertiesTemplate[0]) == 0 ) {
        *priority = name;
    } else if( strcmp(string, personPropertiesTemplate[1]) == 0 ) {
        *priority =  surname;
    } else if( strcmp(string, personPropertiesTemplate[2]) == 0 ) {
        *priority =  birthDate;
    } else if( strcmp(string, personPropertiesTemplate[3]) == 0 ) {
        *priority =  gender;
    } else {
        return false;
    }

    return true;
}

/**
 * Convert a string to order of sort.
 * 
 * @param[in, out] order a pointer to a order of sort.
 * @param[in]      string a constat string.
 * @return true when could find a order of sort to string otherwise false;
 */
bool mapSortOrder(SortOrder *order, const char *string) {
    if( string == NULL ) {
        return false;
    }

    if( strcmp(string, sortOrdersTemplate[0]) == 0 ) {
        *order = ascending;
    } else if( strcmp(string, sortOrdersTemplate[1]) == 0 ) {
        *order = descending;
    } else {
        return false;
    }

    return true;
}

/**
 * Custom compare functions for sorting with Quick sort.
 * 
 * @param[in] a a person.
 * @param[in] b another person.
 * @param[in] filter filter for sortings.
 */
int compareByFilters(Person *a, Person *b, SortFilter filter) {
    int sort = 0;
    switch(filter.priority) {
        case name: 
            sort = strcmp(a->name, b->name);
            break;

        case surname: 
            sort = strcmp(a->surname, b->surname);
            break;

        case birthDate: {
            // Just save date properties as pointers for a better reading conditions.
            int aYear = a->birthDate->tm_year;
            int aMonth = a->birthDate->tm_mon;
            int aDay = a->birthDate->tm_mday;

            int bYear = b->birthDate->tm_year;
            int bMonth = b->birthDate->tm_mon;
            int bDay = b->birthDate->tm_mday;

            if( aYear == bYear &&
                aMonth == bMonth ) {

                if( aDay < bDay ) {
                    sort = -1;
                } else if( aDay > bDay ) {
                    sort = 1;
                } 

            } else if( aYear == bYear ) {

                if( aMonth < bMonth ) {
                    sort = -1;
                } else if( aMonth > bMonth ) {
                    sort = 1;
                }

            } else {

                if( aYear < bYear ) {
                    sort = -1;
                } else if( aYear > bYear ) {
                    sort = 1;
                }

            }
            break;
        }
        case gender: 
            if( a->gender < b->gender ) {
                sort = -1;
            } else if( a->gender > b->gender ) {
                sort = 1;
            }
            break;
        
    }

    if( filter.order == descending ) {
        sort *= (-1);
    }

    return sort;
}

// Global property for quick sort parameters comparator.
// To set sort priority and order.
// This global propert is accesable only from this file. 
// So you can't set or read it from elsewhere.
SortFilter *globalFilters = NULL;
// Global property for count of global properties.
int countOfGlobalFilters = 0;

/*
 * A Quick sort comparator for sorting by global property of filters
 */
int cmp(const void *a, const void *b) {
    Person *personA = (Person*)a;
    Person *personB = (Person*)b;

    for(int i = 0; i < countOfGlobalFilters; ++i) {
        int compareResult = compareByFilters(personA, personB, globalFilters[i]);
        if( compareResult != 0 ) {
            return compareResult;
        }
    }

    return 0;
}

/**
 * Sort an array of persons.
 * 
 * @param[in, out] persons a pointer to an array of persons which need to be sorted.
 * @param[in]      filters an array of filters for sorting.
 * @param[in]      countOfGlobalFilters a count of global fitlers for sorting.
 */
void sortPersons(Person **persons, int countOfPersons, SortFilter *filters, int countOfFilters) {
    if( filters == NULL || !countOfFilters ) {
        return;
    }

    globalFilters = filters;
    countOfGlobalFilters = countOfFilters;
    qsort(*persons, countOfPersons, sizeof(Person), cmp);
}
