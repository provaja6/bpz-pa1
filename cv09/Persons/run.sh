#!/bin/sh

g++ -Wall -pedantic -g main.c -o main.o -c
g++ -Wall -pedantic -g persons.c -o persons.o -c
g++ -g -o persons.exec main.o persons.o

rm -rf main.o
rm -rf persons.o