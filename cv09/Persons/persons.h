#include <time.h>
#include <stdbool.h>

#define DateTime struct tm
#define PERSONS_PROPERTIES_COUNT 4

// A declaration of an enumeration of a gender
typedef enum GENDER { 
    female, 
    male, 
    unknown 
} Gender;

// A global constant of count of genders.
const int countOfGenders = 3;
// A global constant of array of string to genders.
// Use to maping string to gender.
const char gendersTemplate[countOfGenders][8] = {
    "female",
    "male",
    "unknown"
};

/**
 * Convert a string to a enum of gender.
 * 
 * @param[in, out] gender a pointer to a gender where values will be stored.
 * @param[in]      strings an array of pointers to strings where values is looked for.
 * @return true when could get the gender otherwise false.
 */
bool convertStringToGender(Gender *gender, const char *string);

/**
 * Convert a gender to a string.
 * 
 * @param[in, out] gender a unchangeable pointer to a gender.
 * @return a char of the gender.
 */
char convertGenderToString(const Gender *gender);



// A declaration of a struct of a person
typedef struct PERSON {
    char *name;
    char *surname;
    DateTime *birthDate;
    Gender gender;
} Person;

// A global constant of count of person's properties.
const int countOfPersonProperties = 4;
// A global constant of array of string to person's properties.
// Use to maping string to priority of sort.
const char personPropertiesTemplate[countOfPersonProperties][10] = {
    "name",
    "surname",
    "birthDate",
    "gender"
};

/**
 * Get a datetime from a strings
 * 
 * @param[in, out] date a pointer to a date where values will be stored.
 * @param[in]      strings an array of pointers to strings where values is looked for.
 * @return true when could get the datetime otherwise false.
 */
bool retrieveDateTime(DateTime *date, char **strings);

/**
 * Get a person from a strings
 * 
 * @param[in, out] person a pointer to a person where values will be stored.
 * @param[in]      strings an array of pointers to strings where values is looked for.
 * @param[in]      countOfStrings a count of strings. 
 * @return true when could get the person properties otherwise false.
 */
bool retrievePerson(Person *person, char **strings, int countOfStrings);

/**
 * Alloc persons.
 * 
 * @param[in] numberOfPersons a number of persons.
 * @return an array of persons.
 */
Person *allocPersons(int numberOfPersons);

/**
 * Free persons.
 * 
 * @param[in, out] persons a pointer to an array of persons.
 * @param[in]      numberOfPersons a number of persons.
 */
void freePersons(Person **persons, int numberOfPersons);

/**
 * Print formatted person's properties.
 * 
 * @param[in, out] person a constant pointer to a person.
 */
void printPerson(const Person *person);

/**
 * Print formatted persons.
 * 
 * @param[in, out] persons a constant pointer to an array of persons.
 * @param[in]      numberOfPersons a number of persons.
 */
void printPersons(const Person *persons, int numberOfPersons);


// A declaration of an enumeration of a sort priority
typedef enum SORT_PRIORITY {
    name,
    surname,
    birthDate,
    gender
} SortPriority;

/**
 * Convert a string to priority of sort.
 * 
 * @param[in, out] priority a pointer to a priority of sort.
 * @param[in]      string a constat string.
 * @return true when could find a priority of sort to string otherwise false;
 */
bool mapSortPriority(SortPriority *priority, const char *string);

// A declaration of an enumeration of a sort order
typedef enum SORT_ORDER {
    ascending,
    descending
} SortOrder;

// A global constant of count of orders of sort.
const int countOfSortOrders = 2;
// A global constant of array of string to orders of sort.
// Use to maping string to order of sort.
const char sortOrdersTemplate[countOfSortOrders][11] = {
    "ascending",
    "descending"
};

/**
 * Convert a string to order of sort.
 * 
 * @param[in, out] order a pointer to a order of sort.
 * @param[in]      string a constat string.
 * @return true when could find a order of sort to string otherwise false;
 */
bool mapSortOrder(SortOrder *order, const char *string);

// A declaration of struct of a sort filter
typedef struct SORT_FILTER {
    SortPriority priority;
    SortOrder order;
} SortFilter;

/*
 * A Quick sort comparator for sorting by names
 */
int compareByNames(const void *a, const void *b);

/*
 * A Quick sort comparator for sorting by surnames
 */
int compareBySurames(const void *a, const void *b);

/*
 * A Quick sort comparator for sorting by birth dates
 */
int compareByBirthDates(const void *a, const void *b);

/*
 * A Quick sort comparator for sorting by genders
 */
int compareByGenders(const void *a, const void *b);

/**
 * Sort an array of persons.
 * 
 * @param[in, out] persons a pointer to an array of persons which need to be sorted.
 * @param[in]      filters an array of filters for sorting.
 * @param[in]      countOfFilters a count of fitlers for sorting.
 */
void sortPersons(Person **persons, int countOfPersons, SortFilter *filters, int countOfFilters);
